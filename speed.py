#!/bin/env python3
import sys
import numpy as np

data = []
for line in sys.stdin:
    data.append(line.split())

points = np.empty([0,2])
times = np.empty([0,1],dtype='datetime64')
for item in data:
    points = np.vstack([points, np.array([float(item[1]), float(item[2])])])
    times = np.vstack([times, np.datetime64(item[0][:-1])])
    
    
points = points / 180 * np.pi
cosvalue = np.clip(
    np.sin(points[:-1, 0]) * np.sin(points[1:, 0])
    + np.cos(points[:-1, 0])
    * np.cos(points[1:, 0])
    * np.cos(points[:-1, 1] - points[1:, 1]),
    -1,
    1,
    )
dist = 6378 * np.arccos(cosvalue)
cumdist = np.cumsum(dist)
time = times[1:] - times [:-1]

speed = dist/np.squeeze(time.astype('float'))*3.6e6
for i in range(len(speed)):
    print("%s %s %.2f %.1f" % (data[i][0], data[i+1][0], cumdist[i], speed[i]))
