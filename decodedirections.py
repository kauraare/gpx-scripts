#!/bin/env python3

import numpy as np
import sys
import polyline as pl

for line in sys.stdin:
    coords = np.array(pl.decode(line.rstrip()))

t1 = np.datetime64(sys.argv[1])
t2 = np.datetime64(sys.argv[2])
length = coords.shape[0]

data = coords / 180 * np.pi
cosvalue = np.clip(
    np.sin(data[:-1, 0]) * np.sin(data[1:, 0])
    + np.cos(data[:-1, 0]) * np.cos(data[1:, 0]) * np.cos(data[:-1, 1] - data[1:, 1]),
    -1,
    1,
)
dist = 6378 * np.arccos(cosvalue)
frac = np.cumsum(dist) / np.sum(dist)
times = np.array(t1 + (t2 - t1) * frac)


times = np.hstack([times[0], times[:-1] + (times[1:] - times[:-1]) / 2, times[-1]])


for i in range(1, length - 1):
    print(
        '<trkpt lat="%f" lon="%f"><time>%s</time><src>interpolate</src></trkpt>'
        % (*coords[i], times[i])
    )
