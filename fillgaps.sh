#!/usr/bin/env bash
source "$(dirname "$0")/functions.sh"

mode="$1"
gapsfile="$2"

grep "^${mode}$" <(echo bicycling driving walking | tr ' ' '\n') > /dev/null
if [ ! $? -eq 0 ]; then
    echo "First argument must be either 'bicycling', 'walking' or 'driving'"
    exit 1
fi

interpolate() {
  source ~/main/keys/api
  apikey="$google"
  url="https://maps.googleapis.com/maps/api/directions/json?origin=${3}&destination=${4}&mode=${5}&key=${apikey}"
  curl -Ss $url |
      jq -r '.routes[0].overview_polyline.points' |
      "$(dirname "$0")/decodedirections.py" $1 $2 |
      writeAll
}

decide() {
  url="https://www.google.com/maps/dir/${origin}/${destination}"
  echo "Current link is:     $url"
  chromium "$url"

  echo "Accept (y), change coordinates (c), or continue (n)"
  read -n1 choice
  echo -e '\n'

  if [[ $choice =~ ^[Yy]$ ]]; then
      mv "$combined" days
      mv "$orig"     trash
  elif [[ $choice =~ ^[Cc]$ ]]; then
      printf "%s" "Paste new link here: "
      IFS='/' read x1 x2 x3 x4 x5 origin destination x8
      interpolate $gapstart $gapend $origin $destination $mode > "$interpolated"
      combine "$orig" "$interpolated"  |  writeAll > "$combined"
      decide
  elif [[ $choice =~ ^[Nn]$ ]]; then
      rm "$combined"
      rm "$orig"
  else
      decide
  fi
}

cd "$(dirname "$0")/.."
[ -d candidates ] || mkdir candidates
[ -d trash ]      || mkdir trash

while read -u 3 gapstart gapend origin destination; do
    date=$(date -d ${gapstart} -u +%Y%m%d)
    echo -e $gapstart\\n$gapend\\n$origin\\n$destination

    ## make interpolated section and combine
    orig="candidates/${date}_orig.gpx"
    interpolated="candidates/${date}_interpolated.gpx"
    combined="candidates/${date}.gpx"

    cp days/${date}.gpx "$orig"
    interpolate $gapstart $gapend $origin $destination $mode > "$interpolated"
    combine "$orig" "$interpolated"  |  writeAll > "$combined"

    ## Decide whether to accept
    decide
    rm "$interpolated"

done 3< <(cat "$gapsfile" | sort -nr | awk '{print $2 " " $3 " " $4 " " $5}')
