#!/usr/bin/env bash
# usage: gentrips [ input file ]
# Input file is space separated table with columns <tripname, starttime, endtime>.

source "$(dirname "$0")/functions.sh"
srcdir=$HOME/main/tracks/days

[ -d $(dirname "$1")/gpx ]  || mkdir $(dirname "$1")/gpx
[ -d $(dirname "$1")/gaps ] || mkdir $(dirname "$1")/gaps


findLine() {
    upbound=$(cat "$TMPFILE" | wc -l)
    lowbound=1

    while [[ $((${upbound} - ${lowbound})) > 1 ]]; do
        mid=$(((${lowbound}+${upbound})/2))
        lowtime=$(head -n ${lowbound} $TMPFILE | tail -n1 | grep -oP '<time>\K[0-9T:-]+')
        uptime=$(head  -n ${upbound}  $TMPFILE | tail -n1 | grep -oP '<time>\K[0-9T:-]+')
        midtime=$(head -n ${mid}      $TMPFILE | tail -n1 | grep -oP '<time>\K[0-9T:-]+')
        if [[ $1 > $midtime ]]; then
            lowbound=$mid
        else
            upbound=$mid
        fi
    done
    echo $upbound
}

while read trip start end; do
    [[ $trip =~ ^# ]] && continue
    [ -z "$end" ]     && continue

    startdate=$(date -d "${start}" -u +%Y%m%d) || (echo "Invalid date for $trip" && continue)
    enddate=$(date   -d "${end}"   -u +%Y%m%d) || (echo "Invalid date for $trip" && continue)
    gpxoutfile="$(dirname $1)/gpx/${startdate}.${trip}.gpx"
    gapsoutfile="$(dirname $1)/gaps/${startdate}.${trip}.gaps"

    # Check for source files
    TMPFILE=$(mktemp -q /tmp/gentrips.XXXXXX)
    printf "%s.%-20s: " ${startdate} ${trip}

    currentdate=${startdate}
    while [[ $currentdate -le $enddate ]]; do
        infile="$srcdir/${currentdate}.gpx"
        currentdate=$(date -d"$currentdate + 1 day" +"%Y%m%d")
        [ "$infile" -nt "$gpxoutfile" ]  || [[ $(wc -l < "$gpxoutfile") -le 2 ]] && updategpx=true && updategaps=true
        [ "$infile" -nt "$gapsoutfile" ] || [ ! -f "$gapsoutfile" ] && updategaps=true
    done

    # Merge gpx files
    if [ -z $updategpx ]; then
        printf "Skip merging... "
    else
        printf "Merging...      "
        currentdate=${startdate}
        while [[ $currentdate -le $enddate ]]; do
            infile="$srcdir/${currentdate}.gpx"
            currentdate=$(date -d"$currentdate + 1 day" +"%Y%m%d")
            cat "$infile" | grep '^<trkpt' >> $TMPFILE
        done

        startline=$(findLine ${start})
        endline=$(($(findLine ${end})-1))
        length=$((${endline}-${startline}))

        head -n ${endline} "$TMPFILE" | tail -n ${length} | writeTrkseg | writeTrk $trip | writeGpx > "$gpxoutfile"
    fi
    rm "$TMPFILE"

    # Find gaps
    if [ -z $updategaps ]; then
        printf "Skip finding gaps..."
    else
        printf "Finding gaps...     "
        $(dirname "$0")/findgaps.py "$gpxoutfile" > "$gapsoutfile"
    fi
    unset updategpx
    unset updategaps
    printf "\n"
done < "$1"
