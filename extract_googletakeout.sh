#!/usr/bin/env bash

# usage: googletakeout.sh <archive>.tgz
# generates pretty formatted <archive>.gpx

source "$(dirname "$0")/functions.sh"
name=$(basename "$1" .tgz)


if [[ -f "$name.gpx" ]]; then
    echo "$name.gpx exists, skipping"
else
    tar -xOf "$1" --wildcards '*.kml' |
    gpsbabel -i kml -f - -o gpx -F - |
    formatGPX  > "$name.gpx"
fi
