#!/usr/bin/env bash
## Miscellaneous assistive functions for the main scripts

writeTrkseg() {
  echo '<trkseg>'
  cat
  echo '</trkseg>'
}

writeTrk() {
  echo '<trk>'
  if [ -n "$1" ]; then
    echo "<name>$1</name>"
  fi
  cat
  echo '</trk>'
}

writeGpx() {
    echo '<?xml version="1.0" encoding="UTF-8" ?>'
    echo '<gpx version="1.1">'
    cat
    echo '</gpx>'
}

writeAll() {
    writeTrkseg | writeTrk | writeGpx
}

formatGPX() {
    tr -d '\n' |
      sed -E\
          -e 's/>[[:space:]]+</></g'     \
          -e 's|<gpx|<gpx\n|g'           \
          -e 's|<trk|\n<trk|g'           \
          -e 's|</trkseg>|\n</trkseg>|g' \
          -e 's|</trk>|\n</trk>|g'       \
          -e 's|</gpx>|</gpx>\n|g'
}

combine() {
    cat "$@" |
        grep -a '^<trkpt.*</trkpt>$' |
          sed -E 's/.*<time>([0-9TZ:-]+)/\1 &/' |
          sort -u |
          cut -f 2- -d ' '
}

getCleanTrkpts() {
    xmlstarlet fo |
        grep -vE 'src|sat|course|speed|geoidheight|dop' |
        formatGPX |
        grep -a '^<trkpt.*</trkpt>$'
}

gpx2ssv() {
    grep -a '^<trkpt.*</trkpt>$' |
        sed -E 's|.*lat="([0-9.-]+)" lon="([0-9.-]+)".*<time>([0-9TZ:.-]+)</time>.*|\3 \1 \2|'
}

ssv2gpx() {
    sed -E 's|^([0-9TZ:.-]+)[[:space:]]+([0-9.-]+)[[:space:]]+([0-9.-]+)$|<trkpt lat="\2" lon="\3"><time>\1</time></trkpt>|'
}
