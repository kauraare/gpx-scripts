#!/bin/env python3
from pyproj import Geod
import sys
import numpy as np

linecounter = 0
data = []
for line in sys.stdin:
    data.append(line.split())
    linecounter = linecounter + 1
if linecounter != 2:
    print("stdin should have 2 lines")
    sys.exit(1)

geoid = Geod(ellps="WGS84")
N_points = int(sys.argv[1])

points = geoid.npts(
    float(data[0][2]),
    float(data[0][1]),
    float(data[1][2]),
    float(data[1][1]),
    N_points
)

t0 = np.datetime64(data[0][0][:-1])
t1 = np.datetime64(data[1][0][:-1])
frac = np.linspace(0, 1, N_points + 2)
times = np.array(t0 + (t1 - t0) * frac)
times = times[1:-1]

for i in range(len(points)):
    print(str(times[i]) + "Z " + str(points[i][1]) + " " + str(points[i][0]))
