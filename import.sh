#!/usr/bin/env bash
## Looks for gpx files in source directory and separates them into files by date.
## Should remove clutter and validate whether gpx conforms with xml standard.

## usage: import.sh [source path] [destination path] [[archive path]]

( [ $# -eq 2 ] || [ $# -eq 3 ] ) && [ -d "$1" ] && [ -d "$2" ]
if [ $? -ne 0 ] ; then
    echo "Usage: $0 [source path] [destination path] [[archive path]]"
    exit -1
fi

source "$(dirname "$0")/functions.sh"

TMP=$(mktemp -d)
mkdir -p $TMP/{points,merged}

# Extract trkpts from source
cat $(find "$1" -type f | sort) |
    formatGPX |
    grep -a '^<trkpt.*</trkpt>$' |
    awk -F '<time>' -v dir="$TMP" '{print >> dir"/points/"substr($2,1,10)}'

# Write into destination directory
for i in $(ls $TMP/points/*) ; do
    daygpx="$(basename "$i" | tr -d '-').gpx"

    if [ -f "$2/$daygpx" ]; then
        combine "$2/$daygpx" "$i"
    else
        cat "$i"
    fi |
        writeAll > "$TMP/merged/$daygpx"

    mv "$TMP/merged/$daygpx" "$2/$daygpx"
    xmlstarlet val "$2/$daygpx"
done
rm -r "$TMP"


# Add originals to archive
if [ $# -eq 3 ]; then
    find "$1" -name '*.gpx' | sort | zip -r9@j "$3"
    find "$1" -name '*.gpx' | sort | head -n-1 | xargs rm -f
fi
