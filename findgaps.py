#!/bin/env python3
import sys
import numpy as np
import gpxpy
import warnings

warnings.filterwarnings("ignore")

file = sys.argv[1]
gpx_file = open(file, "r")
gpx = gpxpy.parse(gpx_file)

for track in gpx.tracks:
    for segment in track.segments:
        data = np.empty([0, 2])
        time = np.empty(0, dtype="datetime64[us]")
        for point in segment.points:
            new = np.array([[point.latitude, point.longitude]])
            data = np.append(data, new, axis=0)
            time = np.append(time, np.datetime64(point.time))
        data = data / 180 * np.pi
        cosvalue = np.clip(
            np.sin(data[:-1, 0]) * np.sin(data[1:, 0])
            + np.cos(data[:-1, 0])
            * np.cos(data[1:, 0])
            * np.cos(data[:-1, 1] - data[1:, 1]),
            -1,
            1,
        )
        dist = 6378 * np.arccos(cosvalue)
        indices = np.nonzero(dist > 0.1)[0]
        for index in indices:
            print(
                "%5.2f %s %s %f,%f %f,%f %14s"
                % (
                    dist[index],
                    time[index],
                    time[index + 1],
                    *np.reshape(data[index : index + 2] * 180 / np.pi, -1),
                    track.name,
                )
            )
