#!/usr/bin/env bash
# Merges gpx files in $dir/gpx/ and saves the result in $dir/all.gpx
[ $# -ne 1 ]      && 1>&2 echo "Usage: $0 [directory]"               && exit 1
[ ! -d "$1/gpx" ] && 1>&2 echo "$1/gpx directory does not exist" && exit 1

dir="$1"
source "$(dirname "$0")/functions.sh"

TMPFILE=$(mktemp /tmp/gpxmerge.XXXXXX)

for gpx in $(find "$dir/gpx/" -name '*.gpx' -type f | sort); do
    name=$(basename $gpx .gpx)
    echo Merging $name 1>&2
        cat "$gpx" |
            getCleanTrkpts |
            writeTrkseg |
            writeTrk "$name"
done |
    writeGpx > $TMPFILE

mv $TMPFILE "$dir/all.gpx"
